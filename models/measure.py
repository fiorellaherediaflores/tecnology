# -*- coding: utf-8 -*-
from database import Database


class Measure:
    def __init__(self, measure):
        self._name = measure.get('name', False)
        self._code = measure.get('code', False)

    @staticmethod
    def browse(measure_id):
        browse_measure_query = """select * from measure where id = {measure_id}""".format(measure_id=measure_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_measure_query)
        measure = ps_cursor.fetchone()
        return measure

    @staticmethod
    def list_measure():
        browse_product_query = """select * from measure"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        measure = ps_cursor.fetchall()
        return measure

    @property
    def name(self):
        return self._name

    @property
    def code(self):
        return self._code

    @name.setter
    def name(self, name):
        self._name = name

    @code.setter
    def code(self, code):
        self._code = code

    def __repr__(self):
        return "Measure" % self._name